import {Table, Button} from "react-bootstrap";
import { Navigate, Link } from "react-router-dom";
import { useContext, useState, useEffect } from "react"
import UserContext from "../UserContext";

import Swal from "sweetalert2";

export default function UserDashboard(){

	const {user} = useContext(UserContext);

	const [allUser, setAllUser] = useState([]);

		const fetchUserData = () =>{
			fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
				headers: {
					"Authorization": `Bearer ${localStorage.getItem("token")}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				setAllUser(data.map(users => {
					return(
						<tr key={users._id}>
							<td>{users._id}</td>
							<td>{users.firstName}</td>
							<td>{users.lastName}</td>
							<td>{users.mobileNo}</td>
							<td>{users.email}</td>
							<td>{users.purchasedProduct.productId}</td>
							<td>{users.isAdmin ? "Admin" : "Non-Admin"}</td>	
							<td>
								{
									(users.isAdmin)
									?
										<Button variant="danger" size="sm" onClick ={() => nonadmin(users._id, users.email)}>Non-admin</Button>
									:
										<>
											<Button variant="success" size="sm" onClick ={() => admin(users._id, users.email)}>Admin</Button>
										</>

								}
							</td>

						</tr>
					)
				}))

			})
		}


		// Making the user = admin
		const nonadmin = (usersId, usersEmail) =>{
			console.log(usersId)
			console.log(usersEmail)

			fetch(`${process.env.REACT_APP_API_URL}/users/${usersId}`, {
				method: "PATCH",
				headers:{
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem("token")}`
				},
				body: JSON.stringify({
					isAdmin: false
				})
			})
			.then (res => res.json())
			.then (data => {
				console.log(data);

				if (data){
					Swal.fire({
						title: "Succesfully Updated",
						icon: "success",
						text: `${usersEmail} is now non-admin.`
					})
					fetchUserData();
				}
				else {
					Swal.fire({
						title: "Update Unsuccesful!",
						icon: "error",
						text: `Somethine went wrong. Please try again.`
					})
				}

			})
		}

		// Making the user = non-admin
		const admin = (usersId, usersEmail) =>{
			console.log(usersId)
			console.log(usersEmail)

			fetch(`${process.env.REACT_APP_API_URL}/users/${usersId}`, {
				method: "PATCH",
				headers:{
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem("token")}`
				},
				body: JSON.stringify({
					isAdmin: true
				})
			})
			.then (res => res.json())
			.then (data => {
				console.log(data);

				if (data){
					Swal.fire({
						title: "Succesfully Updated",
						icon: "success",
						text: `${usersEmail} is now admin.`
					})
					fetchUserData();
				}
				else {
					Swal.fire({
						title: "Update Unsuccesful!",
						icon: "error",
						text: `Somethine went wrong. Please try again.`
					})
				}

			})
		}


	useEffect(()=>{
		fetchUserData();
	}, [])

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1 className="text-light">Admin Dashboard</h1>
				<Button as={Link} to="/admin" variant="success" size="lg" className="mx-2">Product Details</Button>
			</div>
			<Table striped bordered hover className="bg-white">
		      <thead>
		        <tr>
		          <th>User ID</th>
		          <th>User firstName</th>
		          <th>User lastName</th>
		          <th>Mobile Number</th>
		          <th>Email</th>
		          <th>Purchase Product</th>
		          <th>Status</th>
		          <th>Action</th>
		        </tr>
		      </thead>
		      <tbody>
		      	{ allUser }
		      </tbody>
			</Table>
		</>
		:
		<Navigate to="/products" />
	)
}