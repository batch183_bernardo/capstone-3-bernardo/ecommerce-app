import {useState, useEffect, useContext} from "react";
import UserContext from "../UserContext";

import { Navigate, Link } from "react-router-dom";
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";

export default function Login(){


	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");


	const [isActive, setIsActive] = useState(false);

	function login(e){

		e.preventDefault();


		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);
			console.log(typeof data.access)

			if(typeof data.access !== "undefined"){
				localStorage.setItem("token", data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Valvrave Online Store!"
				});
			}
			else{
				Swal.fire({
					title: "Authetication Failed",
					icon: "error",
					text: "Incorrect Email or Password."
				});
			}

		})


		setEmail("");
		setPassword("");


	}

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	
	useEffect(() =>{
		if(email !== "" && password !== ""){
			setIsActive(true)
		}
		else{
			setIsActive(false)
		}
	}, [email, password])


	return(
		    (user.id !== null)
		    ?	
		    	<Navigate to="/" />
		    :
		    <>
		    	<h1 className="my-5 text-center text-light">Sign in</h1>
		    	<Form id="login"className="row col-md-4 offset-4 border" onSubmit ={(e) => login(e)} >
		    		<Form.Group className="mb-3 mt-3" controlId="userEmail">
		    		  <Form.Label className="text-light">Email address</Form.Label>
		    		  <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
		    		</Form.Group>


		    		<Form.Group className="mb-3" controlId="password" id="myInput">
		    		  <Form.Label className="text-light">Password</Form.Label>
		    		  <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
		    		</Form.Group>


		    		{
		    			isActive
		    			?
		    				<Button variant="primary" type="submit" id="submitBtn" size="sm">
		    				  Submit
		    				</Button>
		    			:
		    				<Button variant="primary" type="submit" id="submitBtn" disabled>
		    				  Submit
		    				</Button>
		    		}
		    		<p className="mt-3 text-center text-light">Not yet registered? <Link to="/registration" className="text-decoration-none">Sign up</Link></p>
		    	</Form>
		    </>
	)
}