import {Table, Button} from "react-bootstrap";
import { Navigate, Link } from "react-router-dom";
import { useContext, useState, useEffect } from "react"
import UserContext from "../UserContext";


import Swal from "sweetalert2";

export default function AdminDashboard(){

	// to validate the user role.
	const {user} = useContext(UserContext);

	//Create allCourses State to contain the courses from the database.
	const [allProducts, setAllProduct] = useState([]);

	//"fetchData()" wherein we can invoke if their is a certain change with the course.

	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
			headers: {
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProduct(data.map(product => {
				return(
					<tr key={product._id}>
						<img src={product.mainImage} width="100px" height="100px" />
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.stocks}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>	
						<td>
							{
								(product.isActive)
								?
									<Button variant="danger" size="sm" onClick ={() => archive(product._id, product.name)}>Archive</Button>
								:
									<>
										<Button variant="success" size="sm" onClick ={() => unarchive(product._id, product.name)}>Unarchive</Button>
										<Button as={Link} to={`/editProduct/${product._id}`} variant="secondary" size="sm">Edit</Button>
									</>

							}
						</td>

					</tr>
				)
			}))

		})
	}

	// Making the course inactive
	const archive = (productId, productName) =>{
		console.log(productId)
		console.log(productName)

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then (res => res.json())
		.then (data => {
			console.log(data);

			if (data){
				Swal.fire({
					title: "Archiving Success!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				fetchData();
			}
			else {
				Swal.fire({
					title: "Archiving Unsuccesful!",
					icon: "error",
					text: `Something went wrong. Please try again.`
				})
			}

		})
	}

	// Making the course active again
	const unarchive = (productId, productName) =>{
		console.log(productId)
		console.log(productName)

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then (res => res.json())
		.then (data => {
			console.log(data);

			if (data){
				Swal.fire({
					title: "Product Succesfully Restored!",
					icon: "success",
					text: `${productName} is now active.`
				})
				fetchData();
			}
			else {
				Swal.fire({
					title: "Unarchiving Unsuccesful!",
					icon: "error",
					text: `Something went wrong. Please try again.`
				})
			}

		})
	}


	// To fetch all courses in the first render of the page.
	useEffect(()=>{
		//invoke fetchData() to get all courses.
		fetchData();
	}, [])

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1 className="text-light">Admin Dashboard</h1>
				<Button as={Link} to="/addProduct" variant="success" size="lg" className="mx-2">Add Products</Button>
				<Button as={Link} to="/userDetails" variant="success" size="lg" className="mx-2">User Details</Button>
			</div>
			<Table striped bordered hover className="bg-white">
		      <thead>
		        <tr className="text-center">
		          <th>Product</th>
		          <th>Product Name</th>
		          <th>Description</th>
		          <th>Price</th>
		          <th>Stocks</th>
		          <th>Status</th>
		          <th>Action</th>
		        </tr>
		      </thead>
		      <tbody>
		      	{ allProducts }
		      </tbody>
			</Table>
		</>
		:
		<Navigate to="/products" />
	)
} 