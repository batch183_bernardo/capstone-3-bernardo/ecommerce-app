import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Card, Button, Row, Col} from "react-bootstrap";

//Deconstruct the "courseProp" form the "props" object to shorten syntax.
export default function ProductCard({productProp}){


	// Deconstruct courseProp properties into their own variable
	const {_id, name, description, price, stocks, mainImage} = productProp;



	return (
		<Card id="ProductCard" className="p-5 my-5">
			<Row>
				<Col md={3}>
				<img className="img-fluid" src={mainImage} width="300px" height="300px"/>
				</Col>
				<Col md={9}>
		    <Card.Body className="text-white">
		        <Card.Title>
		            {name}
		        </Card.Title>
		        <Card.Subtitle>Description: </Card.Subtitle>
		        <Card.Text className="text-white">
		            {description}
		        </Card.Text>
		        <Card.Subtitle>Price: </Card.Subtitle>
		        <Card.Text className="text-white">
		            {price}
		        </Card.Text>
		        <Card.Text className="text-white">
		            Stocks: {stocks}
		        </Card.Text>
		        <Button as={Link} to={`/products/${_id}`} variant="primary">View Details</Button>
		    </Card.Body>
		    	</Col>
		    </Row>
		</Card>
	)
}